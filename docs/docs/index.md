# Homepage

openapi: 3.0.0
info:
  title: API для проката оборудования
  version: 1.0.0
  description: Данный API дает возможность управления прокатом велосипедов и сопутствующего оборудования
servers:
  - url: http://127.0.0.1:8000/api/v1
paths:
  /subjects:
    get:
      summary: Метод получения списка оборудования
      description: >
        Возвращает информацию обо всем оборудовании.
      responses:
        '200':
          description: Успешный ответ со списком всего оборудования
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/bike_responce'
                  
    post:
      summary: Метод создания единицы оборудования
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/bike_responce'
      responses:
          '200':
            description: Созданная единица оборудования
            content:
              application/json:
                schema:
                  type: array
                  items:
                   $ref: '#/components/schemas/bike_responce' 
    
    
  /groups:
    get:
      summary: Метод получения групп оборудования
      description: Возвращает список групп оборудования, новые или бывшие в употреблении
      responses:
        '200':
          description: Успешный ответ со списком групп 
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/group_responce'
                
                
  /subjects_by_pk:
   get:
     summary: Метод получения оборудования по primary key
     description: Возвращает единицу оборудования по указанию ключевого идентификатора
     responses:
        '200':
          description: Успешный ответ c информацией о единице оборудования
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/subject_by_key_responce'
    
                
  /subjects_bu_group:
   get:
     summary: Метод получения оборудования по группу
     description: Возвращает единицу оборудования по признакму принадлежности к группе
     responses:
        '200':
          description: Успешный ответ c информацией о единице оборудования
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/subject_bu_group_responce'
              
              
  /slots:
   get:
    summary: Метод получения слотов на аренду оборудования
    description: Возвращает доступные слоты для аренды
    responses:
        '200':
          description: Успешный ответ c информацией о доступных слотах
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/slots_responce'
   post:
    summary: Метод создания слота аренды
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/slots_responce'
    responses:
        '200':
          description: Созданный слот
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/slots_responce'
                  
   patch:
    summary: Метод обновления информации о слотах
    description: Обновляет информацию о существующих слотах для аренды оборудования
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/slots_responce'
    responses:
      '200':
        description: Успешный ответ с обновленной информацией о слотах
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/slots_responce'
                
                
  /slots_by_pk:
   get:
    summary: Метод получения слотов на аренду оборудования по первичному ключу
    description: Возвращает доступные слоты для аренды на основании введенного первичного ключа
    responses:
        '200':
          description: Успешный ответ c информацией о доступных слотах
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/slots_by_pk_responce'
      
  /get_slots_by_subjet_name:
   get:
    summary: Метод получения слотов на аренду оборудования по наименованию оборудования
    description: Возвращает доступные слоты для аренды на основании введенного наименования оборудования
    responses:
        '200':
          description: Успешный ответ c информацией о доступных слотах
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/get_slots_by_subjet_name'
                
  /images:
    get:
      summary: Метод получения списка изображений
      description: >
        Возвращает все изображения.
      responses:
        '200':
          description: Успешный ответ со всеми изображениями
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/images_responce'  
                
components:
  schemas:
    bike_responce:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object
            properties:
              id:
                type: integer
                description: Уникальный идентификатор оборудования
                format: int64
              title:
                type: string
                description: Бренд и марка оборудования
              description:
                type: string
                description: Технические характеристики оборудования
              booking_price: 
                type: integer
                description: Стоимость бронирования оборудования за единицу времени
              group: 
                type: integer
                description: Характеристики бронируемого оборудовани
              category: 
                type: integer
                description: Тип арендуемого оборудования. Это может быть велосипед, шлем или иные аксессуары
          required:
            - count
            - next
            - previous
            - results
            - id
            - title
            - description
            - booking_price
            - group
            - category
            
            
    group_responce:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object
            properties:
              id:
                type: integer
                description: Уникальный идентификатор оборудования
                format: int64
              title:
                type: string
                description: Бренд и марка оборудования
          required:
            - count
            - next
            - previous
            - results
            - id
            - title
            
            
    subject_by_key_responce:
      type: object
      properties:
            id:
              type: integer
              description: Уникальный идентификатор оборудования
              format: int64
            title:
              type: string
              description: Бренд и марка оборудования
            description:
              type: string
              description: Технические характеристики оборудования
            booking_price: 
              type: integer
              description: Стоимость бронирования оборудования за единицу времени
            group: 
              type: integer
              description: Характеристики бронируемого оборудовани
            category: 
              type: integer
              description: Тип арендуемого оборудования. Это может быть велосипед, шлем или иные аксессуары
              required:
                - id
                - title
                - description
                - booking_price
                - group
                - category
                
                
    subject_bu_group_responce:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object 
          
          required:
            - count
            - next
            - previous
            - results
            
            
    slots_responce:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object
        id:
          type: integer
          description: Идентификационный номер единицы оборудования
        is_available:
          type: boolean
        start_at:
          type: string
          format: date-time
          description: Доступное для начала аренды время в формате ISO 8601
        end_at:
          type: string
          format: date-time
          description: Доступное для окончания аренды время в формате ISO 8601
        is_open_for_book_return:
          type: boolean
        subject:
          type: integer
          description: Количество доступных для аренды единиц оборудования
          
          required:
            - count
            - next
            - previous
            - results
            - id
            - is_available
            - start_at
            - end_at
            - is_open_for_book_return
            - subject
      
    slots_by_pk_responce:
      type: object
      properties:
         is_available:
           type: boolean
         start_at:
           type: string
           format: date-time
           description: Доступное для начала аренды время в формате ISO 8601
         end_at:
           type: string
           format: date-time
           description: Доступное для окончания аренды время в формате ISO 8601
         subject:
           type: integer
           description: Количество доступных для аренды единиц оборудования
      
    get_slots_by_subjet_name:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object 
    
    images_responce:
      type: object
      properties:
        count:
          type: integer
          description: Количество единиц оборудования на текущей странице
        next:
          type: integer
          description: Порядковый номер предыдущей страницы
        previous: 
          type: integer
          description: Порядковый номер следующей страницы
        results: 
          type: array
          items: 
            type: object
